import pandas as pd
import os
import glob
import json
import sys
import requests
from datetime import datetime
import sqlite3

CLIENT_ID = 'aTGkZGRkJKEGXzo9EW8mrsNgwDkZ5pzG'
CLIENT_SECRET = 'Y0kKnMZC3EXU3Ato'
TOKEN_URL = 'https://transact-pre.ti.com/v1/oauth/accesstoken'
GET_FINANCE_DETAIL_URL = 'https://transact-pre.ti.com/v2/backlog/financial-documents?customerPurchaseOrderNumber='

def create_access_token():
    try:
        token_data = {
            'grant_type': 'client_credentials',
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET
        }
        response = requests.post(TOKEN_URL, data=token_data)

        if response.status_code == 200:
            return response.json()['access_token']
        else:
            print('Failed to obtain access token.')
            return None
    except Exception as e:
        print("Error creating access token:", str(e))
        return None


def get_finance_data(file_name,db_file):
    access_token = create_access_token()
    if not access_token:
        return False, "Failed to obtain access token."
    try:
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {access_token}'
        }
        df = pd.read_excel(file_name, engine='openpyxl')
        payloads = []
        filtered_df = df[df['MFG Name'] == 'Texas Instruments']
        grouped = filtered_df.groupby('PO Number')
        conn = sqlite3.connect(db_file)
        cursor = conn.cursor()
        for group_name,group_data in grouped:
            for _, row in group_data.iterrows():
                base_url = GET_FINANCE_DETAIL_URL+str(row['PO Number'])
                print(base_url)
                response = requests.get(base_url, headers=headers)
                print(response.text)
                finance_data = (int(row['PO Number']),int(row['PO Item']),response.text,response.status_code)
                cursor.execute('''
                    INSERT INTO finance_data (PO_Number,PO_Item,response_data,retreive_time,status_code)
                    VALUES (?,?, ?, CURRENT_TIMESTAMP,?)
                ''', finance_data)
                conn.commit()
        cursor.close()
        conn.close()
    except Exception as e:
        error = "{0} : {1}".format(sys.exc_info()[-1].tb_lineno,str(e))
        print(error)
        return False, str(e)
    

def main():
    directory_path = r'C:\Purchase\4-Public\SAPDATA\AUTO_OUSTANDING\2023\SVI\2023_WW37'
    vendor_code = '1502140_'
    matching_files = glob.glob(os.path.join(directory_path, f'{vendor_code}*.txt'))
    if matching_files:
        latest_file = max(matching_files, key=os.path.getmtime)
        # print(f"The latest file with prefix '{vendor_code}' is: {latest_file}")
        # df = pd.read_csv(latest_file, sep=None, engine='python')
        input_file = 'input.xlsx'
        # df.to_excel(ospo_file, index=False)
        db_file = 'orders.db'
        get_finance_data(input_file, db_file)

    else:
        print(f"No files with prefix '{vendor_code}' found in the directory.")


if __name__ == "__main__":
    main()