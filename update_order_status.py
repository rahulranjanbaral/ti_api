import pandas as pd
import os
import glob
import json
import requests
from datetime import datetime
import sqlite3

CLIENT_ID = 'aTGkZGRkJKEGXzo9EW8mrsNgwDkZ5pzG'
CLIENT_SECRET = 'Y0kKnMZC3EXU3Ato'
TOKEN_URL = 'https://transact-pre.ti.com/v1/oauth/accesstoken'
GET_ORDER_DETAIL_URL = 'https://transact-pre.ti.com/v2/backlog/orders?customerPurchaseOrderNumber='

def create_access_token():
    try:
        token_data = {
            'grant_type': 'client_credentials',
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET
        }
        response = requests.post(TOKEN_URL, data=token_data)

        if response.status_code == 200:
            return response.json()['access_token']
        else:
            print('Failed to obtain access token.')
            return None
    except Exception as e:
        print("Error creating access token:", str(e))
        return None


def update_order_status(file_name, db_file):
    access_token = create_access_token()
    if not access_token:
        return False, "Failed to obtain access token."
    try:
        headers = {
            'Authorization': f'Bearer {access_token}'
        }
        df = pd.read_excel(file_name, engine='openpyxl')
        # print(df)
        for index, row in df.iterrows():
            print("updating for PO number ", row['PO Number'])
            base_url = 'https://transact-pre.ti.com/v2/backlog/orders?customerPurchaseOrderNumber=' + str(row['PO Number'])
            response = requests.get(url=base_url, headers=headers)
            print("Status code",response.status_code)
            response_data = response.json()
            new_confirm_date = response_data["orders"][0]['lineItems'][0]['schedules'][0]['confirmations'][0]['estimatedDeliveryDate']
            new_confirm_date = pd.to_datetime(new_confirm_date)
            formatted_date = new_confirm_date.strftime('%d.%m.%Y')
            formatted_date_str = str(formatted_date)
            print(formatted_date_str)
            df.at[index, 'New Confirmed Date'] = new_confirm_date
        df.to_excel(file_name, index=False, engine='openpyxl')
        print("Data updated successfully.")
    except Exception as e:
        return False, str(e)
    
def main():
    directory_path = r'C:\Purchase\4-Public\SAPDATA\AUTO_OUSTANDING\2023\SVI\2023_WW37'
    vendor_code = '1502140_'
    matching_files = glob.glob(os.path.join(directory_path, f'{vendor_code}*.txt'))
    if matching_files:
        # latest_file = max(matching_files, key=os.path.getmtime)
        # print(f"The latest file with prefix '{vendor_code}' is: {latest_file}")
        # df = pd.read_csv(latest_file, sep=None, engine='python')
        ospo_file = 'input1.xlsx'
        # df.to_excel(ospo_file, index=False)
        db_file = 'orders.db'
        update_order_status(ospo_file, db_file)
    else:
        print(f"No files with prefix '{vendor_code}' found in the directory.")


if __name__ == "__main__":
    main()
