import pandas as pd
import os
import glob
import json
import requests
from datetime import datetime
import sqlite3
import sys

CLIENT_ID = 'aTGkZGRkJKEGXzo9EW8mrsNgwDkZ5pzG'
CLIENT_SECRET = 'Y0kKnMZC3EXU3Ato'
TOKEN_URL = 'https://transact-pre.ti.com/v1/oauth/accesstoken'
API_URL_INSERT = 'https://transact-pre.ti.com/v2/backlog/orders'
API_URL_UPDATE = 'https://transact-pre.ti.com/v2/backlog/orders/changeByCustomerPurchaseOrderNumber'
SHIP_TO_ACCOUNT_NUMBER = '184621'

def create_access_token():
    try:
        token_data = {
            'grant_type': 'client_credentials',
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET
        }
        response = requests.post(TOKEN_URL, data=token_data)

        if response.status_code == 200:
            return response.json()['access_token']
        else:
            print('Failed to obtain access token.')
            return None
    except Exception as e:
        print("Error creating access token:", str(e))
        return None


def create_or_change_order(file_name, db_file):
    access_token = create_access_token()
    if not access_token:
        return False, "Failed to obtain access token."
    try:
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {access_token}'
        }
        df = pd.read_excel(file_name, engine='openpyxl')
        payloads = []
        filtered_df = df[df['MFG Name'] == 'Texas Instruments']
        grouped = filtered_df.groupby('PO Number')
        conn = sqlite3.connect(db_file)
        cursor = conn.cursor()
        for group_name, group_data in grouped:
            for _, row in group_data.iterrows():
                line_items = []
                ti_part_number = row['MFG Part']
                customer_currency_code = row['Currency']
                print(row['PO Number'], row['PO Item'],'----')
                if pd.isna(row['Action']):
                    if str(row['SVI Req. Date']) == 'nan':
                        delivery_date = ""
                    else:
                        delivery_date = datetime.strptime(str(row['SVI Req. Date']), "%d.%m.%Y").strftime("%Y-%m-%d")
                    data_to_insert = (row['Plant'],row['Supplier Code'],row['Supplier Name'],row['Purchasing Group'],row['Purchasing Name'],row['Email Address'],\
                                    row['ABC Indicator'],row['Material Group'],row['BOI Group'],row['Material'],row['MFG Part'],row['MFG Name'],\
                                    group_data['Material Desc'].iloc[0], row['Order Date'],int(row['PO Number']), int(row['PO Item']),\
                                    row['PO Schedule Ln'],row['Unit price'],row['Per'],row['Currency'],row['SVI Req. Date'],row['Open Qty'],row['Unit'],\
                                    row['Current ETA SVI'],row['Confirm Qty'],row['Revise Date'],row['Action'],row['New Confirmed Date'],row['Supplier Remark'],\
                                    row['SVI Remark'],row['Intransit Inv'],row['Intransit Qty'],row['Fix.L/T'],row['Aging PO'],row['Supplier Stock'],row['Last Inv'],\
                                    row['MOQ'],row['SPQ'],row['Cancellation'],row['2PL Stock'],row['3PL Stock'],row['Last week result'],row['Delivery text'],\
                                    row['Sort Request Date'],row['Cancel Qty'],row['Confirm Qty(SKU)'],row['Pr.Date Cat'],row['SVI Allow Shipment'],row['Supplier Message'],\
                                    row['New Price'],row['New Per'],row['New Currency'],row['Effective date'],row['MPN Material'],row['Cancel Flag'])
                    cursor.execute("SELECT * FROM purchasing_orders WHERE PO_Number = ? AND PO_Item = ?", (int(row['PO Number']), int(row['PO Item'])))
                    existing_row = cursor.fetchone()
                    print("checking existing record",int(row['PO Number']), int(row['PO Item']))
                    if existing_row:
                        print("updating row when the 'Action' column is empty")
                        line_item = {
                            "customerLineItemNumber": int(row['PO Item']),
                            "lineItemChangeIndicator": 'U',
                            "tiPartNumber": ti_part_number,
                            "customerAnticipatedUnitPrice": row['Unit price'],
                            "quoteNumber": "123456",
                            "customerCurrencyCode": customer_currency_code,
                            "schedules": [
                                {
                                    "requestedQuantity": row['Confirm Qty'],
                                    "requestedDeliveryDate": delivery_date
                                }
                            ]
                        }
                        line_items.append(line_item)
                        payload = {
                        "order": {
                            "customerPurchaseOrderNumber": int(row['PO Number']),
                            "shipToAccountNumber": SHIP_TO_ACCOUNT_NUMBER,
                            "lineItems": line_items
                            }
                        }
                        payloads.append(payload)
                        payload_json = json.dumps(payload)
                        response = requests.post(API_URL_UPDATE, data=payload_json, headers=headers)
                        if response.status_code == 200:
                            response_data = response.json()
                            if "orders" in response_data:
                                orders = response_data["orders"]
                                for order in orders:
                                    order_number = order.get("orderNumber")
                                    if order_number:
                                        ti_order_number = order_number
                                        print(f"Order ID: {order_number}")
                            cursor.execute('''
                                UPDATE purchasing_orders
                                SET 
                                    Plant = ?, Supplier_Code = ?, Supplier_Name = ?, Purchasing_Group = ?, Purchasing_Name = ?,Email_Address = ?,
                                    ABC_Indicator = ?,Material_Group = ?,BOI_Group = ?,Material = ?,MFG_Part = ?,MFG_Name = ?,
                                    Material_Desc = ?, Order_Date = ?, PO_Number = ?, PO_Item = ?,Unit_price = ?, PO_Schedule_Ln = ?, Per = ?,Currency = ?,
                                    SVI_Req_Date = ?,Open_Qty = ?, Unit = ?,Current_ETA_SVI = ?,Confirm_Qty = ?, Revise_Date = ?,
                                    Action = ?,New_Confirmed_Date = ?,Supplier_Remark = ?, SVI_Remark = ?, Intransit_Inv = ?,
                                    Intransit_Qty = ?,Fix_LT = ?,Aging_PO = ?,Supplier_Stock = ?,Last_Inv = ?,MOQ = ?,SPQ = ?,
                                    Cancellation = ?,  _2PL_Stock = ?, _3PL_Stock = ?,Last_week_result = ?,Delivery_text = ?,
                                    Sort_Request_Date = ?,Cancel_Qty = ?,Confirm_Qty_SKU = ?,Pr_Date_Cat = ?,SVI_Allow_Shipment = ?,
                                    Supplier_Message = ?,New_Price = ?,New_Per = ?,  New_Currency = ?,Effective_date = ?,
                                    MPN_Material = ?,Cancel_Flag = ?,TI_Order_Number = ?    
                                WHERE 
                                    PO_Number = ? AND PO_Item = ?
                            ''', data_to_insert + (int(ti_order_number),int(row['PO Number']), int(row['PO Item'])))
                            order_status = response_data["orders"][0]["orderStatus"]
                            trace_data = (order_status,row['MFG Part'],int(ti_order_number), payload_json, json.dumps(response_data), int(response.status_code),int(row['PO Number']), int(row['PO Item']))

                            cursor.execute('''
                                INSERT INTO trace_payload (order_status,MFG_Part,TI_Order_Number, payload, response,current_datetime,status_code,PO_Number,PO_Item)
                                VALUES (?,?,?, ?, ?, CURRENT_TIMESTAMP,?,?,?)
                            ''', trace_data)

                            print("Update successful")
                        else:
                            print(response.text)
                            ti_order_number =""
                            order_status = ""
                            ti_part_number = "" 
                            trace_data = ( payload_json, response.text, int(response.status_code),int(row['PO Number']), int(row['PO Item']))

                            cursor.execute('''
                                INSERT INTO trace_payload ( payload, response,current_datetime,status_code,PO_Number,PO_Item)
                                VALUES ( ?, ?, CURRENT_TIMESTAMP,?,?,?)
                            ''', trace_data)
                            print("POST request failed with status code:", response.status_code)
                            print("Response:", response.text)
                        conn.commit()
                    else:
                        # Row does not exist, perform INSERT
                        print("inserting row when 'Action' is empty and it is a new record")
                        line_item = {
                            "customerLineItemNumber": int(row['PO Item']),
                            "tiPartNumber": ti_part_number,
                            "customerAnticipatedUnitPrice": row['Unit price'],
                            "customerCurrencyCode": customer_currency_code,
                            "schedules": [
                                {
                                    "requestedQuantity": row['Confirm Qty'],
                                    "requestedDeliveryDate": delivery_date
                                }
                            ]
                        }
                        line_items.append(line_item)
                        payload = {
                        "order": {
                            "customerPurchaseOrderNumber": int(group_name),
                            "shipToAccountNumber": SHIP_TO_ACCOUNT_NUMBER,
                            "lineItems": line_items
                            }
                        }
                        payloads.append(payload)
                        payload_json = json.dumps(payload)
                        response = requests.post(API_URL_INSERT, data=payload_json, headers=headers)
                        if response.status_code == 200:
                            response_data = response.json()
                            if "orders" in response_data:
                                orders = response_data["orders"]
                                for order in orders:
                                    order_number = order.get("orderNumber")
                                    if order_number:
                                        ti_order_number = order_number
                                        print(f"Order ID: {order_number}")
                            cursor.execute('''
                                INSERT INTO purchasing_orders (
                                    Plant, Supplier_Code, Supplier_Name, Purchasing_Group, Purchasing_Name, 
                                    Email_Address, ABC_Indicator, Material_Group, BOI_Group, Material, 
                                    MFG_Part, MFG_Name, Material_Desc, Order_Date, PO_Number, PO_Item, 
                                    PO_Schedule_Ln, Unit_price, Per, Currency, SVI_Req_Date, Open_Qty, 
                                    Unit, Current_ETA_SVI, Confirm_Qty, Revise_Date, Action, 
                                    New_Confirmed_Date, Supplier_Remark, SVI_Remark, Intransit_Inv, 
                                    Intransit_Qty, Fix_LT, Aging_PO, Supplier_Stock, Last_Inv, MOQ, 
                                    SPQ, Cancellation, _2PL_Stock, _3PL_Stock, Last_week_result, 
                                    Delivery_text, Sort_Request_Date, Cancel_Qty, Confirm_Qty_SKU, 
                                    Pr_Date_Cat, SVI_Allow_Shipment, Supplier_Message, New_Price, 
                                    New_Per, New_Currency, Effective_date, MPN_Material, Cancel_Flag,TI_Order_Number
                                ) 
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, 
                                        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)
                            ''', data_to_insert+ (int(ti_order_number),))
                            order_status = response_data["orders"][0]["orderStatus"]
                            trace_data = (row['MFG Part'],order_status,int(ti_order_number), payload_json, json.dumps(response_data), int(response.status_code),int(row['PO Number']), int(row['PO Item']))

                            cursor.execute('''
                                INSERT INTO trace_payload (MFG_Part,order_status,TI_Order_Number, payload, response,current_datetime,status_code,PO_Number,PO_Item)
                                VALUES (?,?,?, ?, ?, CURRENT_TIMESTAMP,?,?,?)
                            ''', trace_data)
                        else:
                            response_data = json.loads(response.text)
                            order_status = response_data["orders"][0]["orderStatus"]
                            ti_part_number = response_data["orders"][0]["lineItems"][0]["tiPartNumber"]
                            trace_data = (payload_json, json.dumps(response_data), int(response.status_code),int(row['PO Number']), int(row['PO Item']),ti_part_number,order_status)

                            cursor.execute('''
                                INSERT INTO trace_payload (payload, response,current_datetime,status_code,PO_Number,PO_Item,MFG_Part,order_status)
                                VALUES (?, ?, CURRENT_TIMESTAMP,?,?,?,?,?)
                            ''', trace_data)
                            print("POST request failed with status code:", response.status_code)
                            print("Response:", response.text)
                        conn.commit()
                else:
                    if not pd.isna(row['Action']) and 'cancel' not in str(row['Action']).lower():
                        print(row['Action'])
                        lineItemChangeIndicator = 'U'
                    elif not pd.isna(row['Action']) and 'cancel' in str(row['Action']).lower():
                        print(row['Action'])
                        lineItemChangeIndicator = 'X'
                    data_to_insert = (row['Plant'],row['Supplier Code'],row['Supplier Name'],row['Purchasing Group'],row['Purchasing Name'],row['Email Address'],\
                                    row['ABC Indicator'],row['Material Group'],row['BOI Group'],row['Material'],row['MFG Part'],row['MFG Name'],\
                                    group_data['Material Desc'].iloc[0], row['Order Date'],int(row['PO Number']), int(row['PO Item']),\
                                    row['PO Schedule Ln'],row['Unit price'],row['Per'],row['Currency'],row['SVI Req. Date'],row['Open Qty'],row['Unit'],\
                                    row['Current ETA SVI'],row['Confirm Qty'],row['Revise Date'],row['Action'],row['New Confirmed Date'],row['Supplier Remark'],\
                                    row['SVI Remark'],row['Intransit Inv'],row['Intransit Qty'],row['Fix.L/T'],row['Aging PO'],row['Supplier Stock'],row['Last Inv'],\
                                    row['MOQ'],row['SPQ'],row['Cancellation'],row['2PL Stock'],row['3PL Stock'],row['Last week result'],row['Delivery text'],\
                                    row['Sort Request Date'],row['Cancel Qty'],row['Confirm Qty(SKU)'],row['Pr.Date Cat'],row['SVI Allow Shipment'],row['Supplier Message'],\
                                    row['New Price'],row['New Per'],row['Currency'],row['Effective date'],row['MPN Material'],row['Cancel Flag'])
                    # Check if the row exists in the database
                    cursor.execute("SELECT * FROM purchasing_orders WHERE PO_Number = ? AND PO_Item = ?", (int(row['PO Number']), int(row['PO Item'])))
                    existing_row = cursor.fetchone()
                    print("checking existing record",int(row['PO Number']), int(row['PO Item']))
                    if existing_row:
                        print("updating row when the 'Action' column is  not empty")
                        print
                        if str(row['SVI Req. Date']) == 'nan':
                            delivery_date = ""
                        else:
                            delivery_date = datetime.strptime(str(row['SVI Req. Date']), "%d.%m.%Y").strftime("%Y-%m-%d")
                        line_item = {
                            "customerLineItemNumber": int(row['PO Item']),
                            "lineItemChangeIndicator": lineItemChangeIndicator,
                            "tiPartNumber": ti_part_number,
                            "customerAnticipatedUnitPrice": row['Unit price'],
                            "quoteNumber": "123456",
                            "customerCurrencyCode": customer_currency_code,
                            "schedules": [
                                {
                                    "requestedQuantity": row['Confirm Qty'],
                                    "requestedDeliveryDate": delivery_date
                                }
                            ]
                        }
                        line_items.append(line_item)
                        payload = {
                        "order": {
                            "customerPurchaseOrderNumber": int(row['PO Number']),
                            "shipToAccountNumber": SHIP_TO_ACCOUNT_NUMBER,
                            "lineItems": line_items
                            }
                        }
                        payloads.append(payload)
                        payload_json = json.dumps(payload)
                        response = requests.post(API_URL_UPDATE, data=payload_json, headers=headers)
                        if response.status_code == 200:
                            response_data = response.json()
                            if "orders" in response_data:
                                orders = response_data["orders"]
                                for order in orders:
                                    order_number = order.get("orderNumber")
                                    if order_number:
                                        ti_order_number = order_number
                                        print(f"Order ID: {order_number}")
                            cursor.execute('''
                                UPDATE purchasing_orders
                                SET 
                                    Plant = ?, Supplier_Code = ?, Supplier_Name = ?, Purchasing_Group = ?, Purchasing_Name = ?,Email_Address = ?,
                                    ABC_Indicator = ?,Material_Group = ?,BOI_Group = ?,Material = ?,MFG_Part = ?,MFG_Name = ?,
                                    Material_Desc = ?, Order_Date = ?, PO_Number = ?, PO_Item = ?,PO_Schedule_Ln = ?,Unit_price = ?, Per = ?,Currency = ?,
                                    SVI_Req_Date = ?,Open_Qty = ?, Unit = ?,Current_ETA_SVI = ?,Confirm_Qty = ?, Revise_Date = ?,
                                    Action = ?,New_Confirmed_Date = ?,Supplier_Remark = ?, SVI_Remark = ?, Intransit_Inv = ?,
                                    Intransit_Qty = ?,Fix_LT = ?,Aging_PO = ?,Supplier_Stock = ?,Last_Inv = ?,MOQ = ?,SPQ = ?,
                                    Cancellation = ?,  _2PL_Stock = ?, _3PL_Stock = ?,Last_week_result = ?,Delivery_text = ?,
                                    Sort_Request_Date = ?,Cancel_Qty = ?,Confirm_Qty_SKU = ?,Pr_Date_Cat = ?,SVI_Allow_Shipment = ?,
                                    Supplier_Message = ?,New_Price = ?,New_Per = ?,  New_Currency = ?,Effective_date = ?,
                                    MPN_Material = ?,Cancel_Flag = ?,TI_Order_Number = ?    
                                WHERE 
                                    PO_Number = ? AND PO_Item = ?
                            ''', data_to_insert + (int(ti_order_number),int(row['PO Number']), int(row['PO Item'])))
                            order_status = response_data["orders"][0]["orderStatus"]
                            trace_data = (order_status,row['MFG Part'],int(ti_order_number), payload_json, json.dumps(response_data), int(response.status_code),int(row['PO Number']), int(row['PO Item']))

                            cursor.execute('''
                                INSERT INTO trace_payload (order_status,MFG_Part,TI_Order_Number, payload, response,current_datetime,status_code,PO_Number,PO_Item)
                                VALUES (?,?,?, ?, ?, CURRENT_TIMESTAMP,?,?,?)
                            ''', trace_data)
                            print("Update successful")
                        else:
                            response_data = json.loads(response.text)
                            ti_order_number = response_data["orders"][0]["orderNumber"]
                            order_status = response_data["orders"][0]["orderStatus"]
                            trace_data = (order_status,row['MFG Part'],int(ti_order_number), payload_json, json.dumps(response_data), int(response.status_code),int(row['PO Number']), int(row['PO Item']))

                            cursor.execute('''
                                INSERT INTO trace_payload (order_status,MFG_Part,TI_Order_Number, payload, response,current_datetime,status_code,PO_Number,PO_Item)
                                VALUES (?,?,?, ?, ?, CURRENT_TIMESTAMP,?,?,?)
                            ''', trace_data)
                            print("POST request failed with status code:", response.status_code)
                            print("Response:", response.text)
                        conn.commit()
                    else:
                        print("inserting row when 'Action' is not empty and it is a new record")
                        if str(row['SVI Req. Date']) == 'nan':
                            delivery_date = ""
                        else:
                            delivery_date = datetime.strptime(str(row['SVI Req. Date']), "%d.%m.%Y").strftime("%Y-%m-%d")
                        line_item = {
                            "customerLineItemNumber": int(row['PO Item']),
                            "tiPartNumber": ti_part_number,
                            "customerAnticipatedUnitPrice": row['Unit price'],
                            "customerCurrencyCode": customer_currency_code,
                            "schedules": [
                                {
                                    "requestedQuantity": row['Confirm Qty'],
                                    "requestedDeliveryDate": delivery_date
                                }
                            ]
                        }
                        line_items.append(line_item)
                        payload = {
                        "order": {
                            "customerPurchaseOrderNumber": int(group_name),
                            "shipToAccountNumber": SHIP_TO_ACCOUNT_NUMBER,
                            "lineItems": line_items
                            }
                        }
                        payloads.append(payload)
                        payload_json = json.dumps(payload)
                        response = requests.post(API_URL_INSERT, data=payload_json, headers=headers)
                        if response.status_code == 200:
                            response_data = response.json()
                            if "orders" in response_data:
                                orders = response_data["orders"]
                                for order in orders:
                                    order_number = order.get("orderNumber")
                                    if order_number:
                                        ti_order_number = order_number
                                        print(f"Order ID: {order_number}")
                            cursor.execute('''
                                INSERT INTO purchasing_orders (
                                    Plant, Supplier_Code, Supplier_Name, Purchasing_Group, Purchasing_Name, 
                                    Email_Address, ABC_Indicator, Material_Group, BOI_Group, Material, 
                                    MFG_Part, MFG_Name, Material_Desc, Order_Date, PO_Number, PO_Item, 
                                    PO_Schedule_Ln, Unit_price, Per, Currency, SVI_Req_Date, Open_Qty, 
                                    Unit, Current_ETA_SVI, Confirm_Qty, Revise_Date, Action, 
                                    New_Confirmed_Date, Supplier_Remark, SVI_Remark, Intransit_Inv, 
                                    Intransit_Qty, Fix_LT, Aging_PO, Supplier_Stock, Last_Inv, MOQ, 
                                    SPQ, Cancellation, _2PL_Stock, _3PL_Stock, Last_week_result, 
                                    Delivery_text, Sort_Request_Date, Cancel_Qty, Confirm_Qty_SKU, 
                                    Pr_Date_Cat, SVI_Allow_Shipment, Supplier_Message, New_Price, 
                                    New_Per, New_Currency, Effective_date, MPN_Material, Cancel_Flag,TI_Order_Number
                                ) 
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, 
                                        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)
                            ''', data_to_insert+(int(ti_order_number),))
                            order_status = response_data["orders"][0]["orderStatus"]
                            trace_data = (order_status,row['MFG Part'],int(ti_order_number), payload_json, json.dumps(response_data), int(response.status_code),int(row['PO Number']), int(row['PO Item']))

                            cursor.execute('''
                                INSERT INTO trace_payload (order_status,MFG_Part,TI_Order_Number, payload, response,current_datetime,status_code,PO_Number,PO_Item)
                                VALUES (?,?,?, ?, ?, CURRENT_TIMESTAMP,?,?,?)
                            ''', trace_data)
                        else:
                            response_data = json.loads(response.text)
                            order_status = response_data["orders"][0]["orderStatus"]
                            ti_part_number = response_data["orders"][0]["lineItems"][0]["tiPartNumber"]
                            trace_data = (payload_json, json.dumps(response_data), int(response.status_code),int(row['PO Number']), int(row['PO Item']),ti_part_number,order_status)

                            cursor.execute('''
                                INSERT INTO trace_payload (payload, response,current_datetime,status_code,PO_Number,PO_Item,MFG_Part,order_status)
                                VALUES (?, ?, CURRENT_TIMESTAMP,?,?,?,?,?)
                            ''', trace_data)
                            print("POST request failed with status code:", response.status_code)
                            print("Response:", response.text)
                        conn.commit()
        cursor.close()
        conn.close()
        with open('output.json', 'w') as json_file:
            json.dump(payloads, json_file, indent=2)
        return True, "success"
    except Exception as e:
        error = "{0} : {1}".format(sys.exc_info()[-1].tb_lineno,str(e))
        print(error)
        return False, str(e)


def main():
    directory_path = r'C:\Purchase\4-Public\SAPDATA\AUTO_OUSTANDING\2023\SVI\2023_WW37'
    vendor_code = '1502140_'
    matching_files = glob.glob(os.path.join(directory_path, f'{vendor_code}*.txt'))
    if matching_files:
        latest_file = max(matching_files, key=os.path.getmtime)
        # print(f"The latest file with prefix '{vendor_code}' is: {latest_file}")
        # df = pd.read_csv(latest_file, sep=None, engine='python')
        ospo_file = 'input.xlsx'
        # df.to_excel(ospo_file, index=False)
        db_file = 'orders.db'
        success, response_data = create_or_change_order(ospo_file, db_file)
        if success:
            print(response_data)
        else:
            print("Error:", response_data)
    else:
        print(f"No files with prefix '{vendor_code}' found in the directory.")


if __name__ == "__main__":
    main()
