import pandas as pd
import requests
import shutil
from django.core.mail.message import EmailMessage
from django.conf import settings

settings.configure(
    DEBUG=False,
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend',
    EMAIL_HOST = '12.1.2.31',
    EMAIL_PORT = 25,
    EMAIL_USE_TLS = False,
    EMAIL_HOST_USER = '',
    EMAIL_HOST_PASSWORD = '',
    DEFAULT_FROM_EMAIL = 'noreply@svi.co.th',
)


def get_quote_price(part_number,row):

    headers = {}
    base_url = 'https://transact.ti.com/v2/store/products/'
    url = f'{base_url}{part_number}'
    params = {'currency': 'USD', 'exclude-evms':'true'}
    headers['Authorization'] = '1234abcxyz'
    volume = row['Volume Vol. #1']
    response_data = {
                "tiPartNumber": "AFE7799IABJ",
                "genericPartNumber": "AFE7799",
                "buyNowUrl": "https://www.ti.com/product/AFE7799/part-details/AFE7799IABJ",
                "quantity": 5435,
                "limit": 50,
                "pricing": [
                    {
                        "currency": "USD",
                        "priceBreaks": [
                            {"priceBreakQuantity": 1, "price": 2.03},
                            {"priceBreakQuantity": 10, "price": 1.43},
                            {"priceBreakQuantity": 25, "price": 1.35},
                            {"priceBreakQuantity": 100, "price": 1.1},
                            {"priceBreakQuantity": 200, "price": 1},
                            {"priceBreakQuantity": 350, "price": 0.95},
                            {"priceBreakQuantity": 600, "price": 0.9},
                            {"priceBreakQuantity": 750, "price": 0.50},
                        ]
                    }
                ],
                "futureInventory": [
                    {"forecastQuantity": 3500, "forecastDate": "2023-06-04"},
                    {"forecastQuantity": 5000, "forecastDate": "2023-06-11"},
                    {"forecastQuantity": 2000, "forecastDate": "2023-06-18"},
                ],
                "description": "8-Bit 200MSPS Low-Power Analog-to-Digital Converter (ADC) With Internal Sample and Hold",
                "minimumOrderQuantity": 1,
                "standardPackQuantity": 126,
                "exportControlClassificationNumber": "EAR99",
                "htsCode": "8542390001",
                "pinCount": 5,
                "packageType": "SOT-23 (DBV)",
                "packageCarrier": "Large T&R",
                "customReel": True,
                "lifeCycle": "ACTIVE",
            }
            
    try:
        # response = requests.get(url, params=params, headers=headers)
        # if response.status_code == 200:
        #     response_data = response.json()
        #     nearest_price = None
        #     for item in response_data['pricing'][0]['priceBreaks']:
        #         if volume >= item['priceBreakQuantity']:
        #             nearest_price = item['price']
        #         else:
        #             break
        #     return nearest_price
        # elif response.status_code == 401:
        #     print("401 Unauthorized: Check your API authorization.")
        # elif response.status_code == 404:
        #     print("404 Not Found: The server could not be found.")
        # elif response.status_code == 500:
            # print("500 Internal Server Error.")
        nearest_price = None
        for item in response_data['pricing'][0]['priceBreaks']:
            if volume >= item['priceBreakQuantity']:
                nearest_price = item['price']
            else:
                break
        return nearest_price
    except Exception as e:
        # print(f"An error occurred: {e} with response code {response.status_code}")
        return None


def send_email(subject, body, filename):
    sender_email = 'noreply@svi.co.th'
    recipient_email = "rahul.ran@svi.co.th"
    email = EmailMessage(subject, body, sender_email, [recipient_email])
    with open(filename, 'rb') as attachment:
        email.attach(filename, attachment.read(), 'application/octet-stream')
    email.send()


def main():
    excel_file_path = 'pricing_sheet.xlsx'
    copy_file_path = 'pricing_sheet_updated.xlsx'
    shutil.copy(excel_file_path, copy_file_path)

    df = pd.read_excel(copy_file_path, engine='openpyxl')
    print(df)

    for index, row in df.iterrows():
        print(row['Mfg Part Number'])
        volume = row['Volume Vol. #1']
        print(row['Customer Name'])
        print(row['Application'])
        nearest_price = get_quote_price(row['Mfg Part Number'],row)
        df.at[index, 'Price Vol. #1'] = nearest_price
        if nearest_price is not None:
            print(f"The price for volume {volume} is: {nearest_price}")
        else:
            print("Volume is lower than all price break quantities.")
    df.to_excel(copy_file_path, engine='openpyxl', index=False)
    email_subject = "Pricing Sheet Updated"
    email_body = "Pricing Sheet Updated!!!. Attached is the updated pricing sheet."
    send_email(email_subject, email_body, copy_file_path)

if __name__ == "__main__":
    main()
